var Campground = require("../models/campground");
// var Comment = require("../models/comment");

var middlewareObj = {};

middlewareObj.checkCampgroundOwnership = function(req,res,next) {
        if (req.isAuthenticated()){
            Campground.findById(req.params.id, function(err,foundCampground){
                if(err){
                    req.flash("error","Campground not found");
                    res.redirect("back")
                }else{
                    //does user own the campground?
                    if (String(foundCampground.author.id)==(req.user._id)){
                        next();
                    }else{
                        req.flash("error","Access denied");
                        res.redirect("back");
                    }
                }
            });
        }else{
            req.flash("error"," Login required !");
            res.redirect("back");
        }
    }

// middlewareObj.checkCommentOwnership = function(){

// }

middlewareObj.isLoggedIn = function(req,res,next){
    if (req.isAuthenticated()){
        return next();
    }
    req.flash("error"," Login required !");
    res.redirect("/login");
}

module.exports = middlewareObj;