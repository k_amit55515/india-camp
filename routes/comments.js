var express = require("express");
var Router = express.Router();
var Campground = require("../models/campground");
var Comment = require("../models/comment");
var middleware = require("../middleware");

Router.get("/campgrounds/:id/comments/new",middleware.isLoggedIn,function(req,res){
    Campground.findById(req.params.id, function(err,campground){
        if (err){
            console.log(err);
        }else{
            res.render("comments/new", {campground: campground});
        }
    });
});

Router.post("/campgrounds/:id/comments",middleware.isLoggedIn, function(req,res){
    //lookup campground using id
    Campground.findById(req.params.id, function(err,campground){
        if (err){
            console.log(err)
            res.redirect("/campgrounds");
        }else{
            // console.log(req.body.comments);
            Comment.create(req.body.comment, function(err, comment){
                if (err){
                    console.log(err);
                }else{
                    // add usernam and id to comment
                      comment.author.id = req.user._id,
                      comment.author.username = req.user.username;
                    // save comment
                    comment.save();
                    campground.comments.push(comment);
                    campground.save();
                    res.redirect('/campgrounds/'+ campground._id);
                }
            });
        }
    });
});

Router.get("/campgrounds/:id/comments/:comment_id/edit",function(req,res){
    // res.send("hello");

    Comment.findById(req.params.comment_id, function(err,foundComment){
        if(err){
            res.redirect("back");
        }else{
            res.render("comments/edit",{campground_id: req.params.id, comment:foundComment});
        }
    });
});

Router.put("/campgrounds/:id/comments/:comment_id",function(res,req){
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comments ,function(err,updatedComment){
        if(err){
            res.redirect("back");
        }else{
            res.redirect("/campgrounds/"+req.params.id);
        }
    });
});

module.exports = Router;