var express = require("express");
var Router = express.Router();
var Campground = require("../models/campground");

var middleware = require("../middleware");

Router.get("/",function(req,res){
    res.render("landing");
});

// index show all campgrounds
Router.get("/campgrounds",function(req,res){
    //get all campgrounds from db

    Campground.find({}, function(err, allCampgrounds){
        if (err){
            console.log(err);
        } else {
            res.render("index", {campgrounds : allCampgrounds})
        }
    });
});

Router.post("/campgrounds",middleware.isLoggedIn,function(req, res){
    //get data from form and add to campground array
    var name = req.body.name
    var price = req.body.price
    var image = req.body.image
    var desc = req.body.description
    var author = {
        id: req.user._id,
        username: req.user.username
    }

    var newCampground = { name: name,price:price ,image: image, description: desc, author:author}
    
    //create a new campground and save to DB
    Campground.create(newCampground,function(err,newlyCreated){
        if(err){
            console.log(err);
        }else{
            //redirect back to campground page
            console.log(newlyCreated);
            res.redirect("/campgrounds");
        }
    });
});

Router.get("/campgrounds/new",middleware.isLoggedIn, function(req,res){
    
    res.render("new.ejs");
});
//show more info about each item
Router.get("/campgrounds/:id",function(req,res){
    Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground){
        if(err){
            console.log(err);
        }else{
            console.log(foundCampground);
            res.render("show",{campground: foundCampground });
        }
    });
});

function isLoggedIn(req,res,next){
    if (req.isAuthenticated()){
        return next();
    }
    res.redirect("/login");
}
//edit campground route
Router.get("/campgrounds/:id/edit",middleware.checkCampgroundOwnership,function(req,res){
    Campground.findById(req.params.id, function(foundCampground){
        res.render("./edit",{campground: foundCampground});
    });   
});


Router.put("/campgrounds/:id",middleware.checkCampgroundOwnership,function(req,res){
    Campground.findByIdAndUpdate(req.params.id,req.body.campground,function(err,updatedCampground){
        if (err){
            res.redirect("/campgrounds");
        }else{
            res.redirect("/campgrounds/"+ req.params.id);
        }
    });
});
//destroy campground route
Router.delete("/campgrounds/:id",middleware.checkCampgroundOwnership,function(req,res){
    Campground.findByIdAndRemove(req.params.id, function(err){
        if(err){
            res.redirect("/campgrounds");
        }else{
            res.redirect("/campgrounds");
        }
        
    });
})


module.exports = Router;